library(xts)
library(segclust2d)
library(lubridate)
library(extraDistr)
library(ggplot2)




chr_lengths <- list(249250621,243199373,198022430,191154276,180915260,171115067,159138663,146364022,141213431,135534747,135006516,133851895,115169878,107349540,102531392,90354753,81195210,78077248,59128983,63025520,48129895,51304566,155270560,59373566)
names(chr_lengths) <- c('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','X','Y')
chrs = names(chr_lengths)

STEP=500


fill_NA_CN<-function(major_clone,minor_clone){
	for (j in 1:nrow(minor_clone)){
		if (anyNA(minor_clone[j,])){
			minor_clone[j,]=major_clone[j,]
		}
	}
	return(minor_clone)
}

read_battenberg_output<-function(subclones_txt_file){
	subclones = read.csv(subclones_txt_file,sep="\t",header=TRUE)
	sample_chr_start_end = cbind( rep('SAMPLE',nrow(subclones)),subclones[,c(1,2,3)])
	cn_columns = grep("n.*_A", colnames(subclones), perl=TRUE, value=FALSE)
	subclones_CN=subclones[,cn_columns]
	number_of_clones = ncol(subclones_CN)/2
	
	final_colnames = c('sample','chr','startpos','endpos','nMajor','nMinor')
	subclones_list=list()
	
	major_clone_cols=c(1,2)
	major_clone = subclones_CN[,major_clone_cols]
	major_clone = cbind(sample_chr_start_end,major_clone)
	
	colnames(major_clone)= final_colnames
	subclones_list[[1]]=major_clone

	for (i in 2:number_of_clones){
		minor_clone = subclones_CN[,major_clone_cols  +2]
		minor_clone = cbind(sample_chr_start_end,minor_clone)
		colnames(minor_clone)=final_colnames
		minor_clone_noNA=fill_NA_CN(major_clone,minor_clone)
		subclones_list[[i]]=minor_clone_noNA
	}
	
	return (subclones_list)
}

delete<-function(fn){
	if (file.exists(fn)){
 	 	file.remove(fn)
	}  
}

process_chr<-function(chr,data){
	
	if (length(which(data[,2] == chr)) == 0){
		return (NULL)
	}
	
	
	small_chrs=c(19,20,21,22)
	if (chr %in% small_chrs ){
		STEP=100
	}

	this_chr=data[which(data[,2]==chr),][,-1]

	sink(paste('chr',chr,'.txt',sep=''))
	for (i in 1:nrow(this_chr)){
		start=this_chr[i,2]
		end=this_chr[i,3]
		cna=this_chr[i,4]
		cnb=this_chr[i,5]

		if ( (cna > 3) | (cnb > 3) ){
			STEP=1000
		}

		if ( (cna > 5) | (cnb > 5) ){
			next
		}
		
		if (start < end){
			bases_seq=seq(start,end,STEP)
			cna_seq=rep(cna,length(bases_seq))
			cnb_seq=rep(cnb,length(bases_seq))
			for (k in 1:length(bases_seq)){
				cat(paste(bases_seq[k],cna_seq[k],cnb_seq[k],"\n",sep=" "))
			}
		}
	}
	sink()
}


plot_chr_segments<-function(chr_segments,colour,extra){

	for(i in 1:length(chr_segments)){
		segment=chr_segments[[i]]
		chr=segment[1]
		start=segment[2]
		end=segment[3]
		cn=as.numeric(segment[4])+extra
		lines(c(start,end),c(cn,cn),col = colour,cex=1)
	}
}

join_chr_segments<-function(chr_segments,segment_a_index,segment_b_index){
	if (segment_b_index > length(chr_segments)){
		return(chr_segments)
	}

	this_segment=chr_segments[[segment_a_index]]
	next_segment=chr_segments[[segment_b_index]]
	cn_this_segment=this_segment[4]
	cn_next_segment=next_segment[4]

	if (cn_this_segment == cn_next_segment){
		chr = this_segment[1]
		start = this_segment[2]
		end = next_segment[3]
		cn = cn_this_segment
		super_segment = c(chr,start,end,cn)
		
		chr_segments[[ segment_a_index ]]=super_segment
		chr_segments[[ segment_b_index ]]=NULL
		segment_b_index=segment_b_index - 1
		
		if (length(chr_segments) == 1){
            return(chr_segments)
        }

		chr_segments=join_chr_segments(chr_segments,segment_a_index,segment_b_index)
	}else{
		segment_a_index=segment_b_index
		segment_b_index=segment_b_index+1
		chr_segments=join_chr_segments(chr_segments,segment_a_index,segment_b_index)
	}
}


args = commandArgs(trailingOnly=TRUE)

if (args[1]=='--battenberg'){
	subclones_list = read_battenberg_output(args[2])
	for (i in 1:length(subclones_list)){
		new_dir=paste(getwd(),'/','subclone_',i,sep='')
		print(new_dir)
		dir.create(new_dir)
		setwd(new_dir)
		seg=subclones_list[[i]]
		sapply( as.matrix(chrs),process_chr,data=seg)
		apply(as.matrix(chrs),1,segment_chr)
		setwd(paste(new_dir,'/..',sep=''))
	}
}else if(args[1]=='--ascat'){
	ascat= load(args[2]) 
	ascat= load(ascat)
	sapply(as.matrix(chrs),process_chr,data=seg)
	apply(as.matrix(chrs),1,segment_chr)
}else{
	print(paste('unknown: ', args[1], sep=''))
	print(paste('Rscript --vanilla $PATH_TO/ASCAT_postsegmentation.R --ascat R.data',sep=''))
	print(paste('Rscript --vanilla $PATH_TO/ASCAT_postsegmentation.R --battenberg subclones.txt',sep=''))
}

