# Smoothy
<img src="images/logo.png" width="20%" height="20%">

## This code takes as input the ASCAT .Rdata file (or Battenberg _subclones.txt), reads the segmentation 'seg' object in memory and runs another segmentation on top of it.
[code](ASCAT_postsegmentation.R)

As a result you can simplify the segmentation ASCAT object and its plot, which is actually done by concatenating points to simulate segments.
This schema shows what the code does. It takes the CN segments of chromosome twelve and turn then into the main segments you see in the segmentation plot.

![summary](images/ASCAT_postsegmentation_images_summary.png)

### To use the code as it is now you wuill need to be able to load these libraries on the R (3.6.0) console:

    library(xts)
    library(segclust2d)
    library(lubridate)
    library(extraDistr)
    library(ggplot2)

### I run it like this to process ASCAT .Rdata file:

    module load gcc/6.4.0 R/3.6.0
    cd $OUTPUTDIR
    Rscript --vanilla --ascat $PATH_TO/ASCAT_postsegmentation.R LFS2-tumor-MCRL003.Rdata
    
### I run it like this to process Battenberg _subclones.txt file:

    module load gcc/6.4.0 R/3.6.0
    cd $OUTPUTDIR
    Rscript --vanilla --battenberg $PATH_TO/ASCAT_postsegmentation.R LFS2-tumor-MCRL003_subclones.txt
    

### It takes like twenty minutes to run, give it 30G RAM. The output you want to look at is a series of chr_A.CHR.segments.txt chr_B.CHR.segments.txt tsv files with the segmentation (chr start end CN) like this:

    cat chr_A.12.segments.txt
    chr	start	end	CN
    12 68657 34733605 2
    12 34764437 133794428 1
    
    cat chr_B.12.segments.txt
    chr	start	end	CN
    12 68657 46939153 1
    12 47001145 133794428 0

### There is also one plot per chr (CHR.pdf) like this one:
![CHR12](images/ASCAT_postsegmentation_images_chr12.png)

### In the case of processing Battenberg _subclones.txt file the same information will be produced by subclone in 'subclone_X' folders.

